package com.cursjava.academia2;

import java.util.ArrayList;
import java.util.List;

import com.cursjava.academia2.controladores.AlumnoController;
import com.cursjava.academia2.controladores.CursoController;
import com.cursjava.academia2.modelos.Alumno;
import com.cursjava.academia2.modelos.Curso;
import com.cursjava.academia2.vistas.alumnos.Lista;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        List<Alumno> llistat = AlumnoController.getAll();
        Lista.mostrarCursos(llistat);

        // Curso curs = CursoController.getId(3);
        // System.out.println("Curso: "+ curs.getNombre());
        // System.out.println("-------------------");
        // llistat = AlumnoController.getAllCurso(curs);
        // Lista.mostrarCursos(llistat);

        // Alumno al = AlumnoController.getId(1);
        // System.out.println("alumne 1:");
        // System.out.println(al.toString());

        // System.out.println("modificant alumne: ");
        // Alumno al1 = AlumnoController.getId(5);
        // al1.setEmail("XXXXXXX");
        // System.out.println(al1);
        // AlumnoController.save(al1);

        AlumnoController.removeId(5);
        
    }
}
