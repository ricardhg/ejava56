
package com.cursjava.academia2.vistas.alumnos;

import java.util.List;

import com.cursjava.academia2.controladores.CursoController;
import com.cursjava.academia2.modelos.Alumno;
import com.cursjava.academia2.modelos.Curso;

public class Lista {

    public static void mostrar(List<Alumno> listado) {

        for (Alumno al : listado) {
            System.out.println(al.toString());
        }

    }

    public static void mostrarCursos(List<Alumno> listado) {

        for (Alumno al : listado) {
            List<Curso> cursos = CursoController.getAllAlumno(al);
            StringBuilder sb = new StringBuilder();

            for (Curso c : cursos){
                sb.append(c.getNombre());
                sb.append(", ");
            }
            System.out.println(al.toString()+" cursos: "+sb.toString());
        }

    }


}