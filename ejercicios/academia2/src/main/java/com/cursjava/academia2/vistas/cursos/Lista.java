
package com.cursjava.academia2.vistas.cursos;

import java.util.List;

import com.cursjava.academia2.modelos.Curso;

public class Lista {

    public static void mostrar(List<Curso> listado) {

        for (Curso al : listado) {
            System.out.println(al.toString());
        }

    }

}