package com.cursjava.academia2.controladores;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cursjava.academia2.DBConn;
import com.cursjava.academia2.modelos.Alumno;
import com.cursjava.academia2.modelos.Curso;
import com.mysql.jdbc.Connection;

public class CursoController {

    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "cursos";
	private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Curso> getAll(){
        
        List<Curso> listaCursos = new ArrayList<Curso>();
		String sql = String.format("select %s,nombre from %s", KEY, TABLE);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Curso u = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre")
                );
                listaCursos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaCursos;

    }


    // getId devuelve todos los registros de la tabla
    public static Curso getId(int idalumno){
        
        Curso al = null;
		String sql = String.format("select %s,nombre from %s where id=%d", KEY, TABLE, idalumno);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                al = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre")
                );
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return al;

    }


    public static List<Curso> getAllAlumno(Alumno al){
 
        List<Curso> listaCursos = new ArrayList<Curso>();
        String sql = String.format("select c.* from matriculas m, cursos c where m.cursos_id=c.id and m.alumnos_id=%d",
                                         al.getId());

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Curso u = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre")
                );
                listaCursos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaCursos;
    }





}