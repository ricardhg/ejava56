package com.cursjava.academia2.modelos;

public class Curso {

    private int id;
    private String nombre;
   
    public Curso(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }

    public Curso(String nombre){
        this.nombre = nombre;
    }

    public Curso(){}



    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        
        return this.nombre + " (" + this.id +")";
        
    }




    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    
}