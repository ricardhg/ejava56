import java.util.Arrays;
import com.google.gson.Gson;

// COMPILAR I EXECUTAR AIXi:
//      javac -classpath "gson.jar;." *.java
//      java -classpath "gson.jar;." Test

class Test {

    public static void main(String[] args) {
        
        Gson gson = new Gson();
        Estacio[] estacions;


        try {
           String jsonstr =  Bicing.getData();
           if (jsonstr!=null){
            estacions = gson.fromJson(jsonstr, Estacio[].class);
            for (Estacio e : estacions){
                System.out.println(e);
            }
           }
           
           
        }catch(Exception e){
            //...
        }

    }


}