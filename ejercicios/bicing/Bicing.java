import java.io.*;
import java.net.*;



class Bicing {

    public static final String BICINGURL = "http://api.citybik.es/bicing.json";

    public static String getData() {
        URL url;
        URLConnection conn;
        try {
            url = new URL(BICINGURL);
            conn = url.openConnection();
        } catch (IOException e) {
            System.out.println("URL no vàlida o connexió no disponible.");
            return null;
        }


        try ( 
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringWriter response = new StringWriter() ) {

            int size = 1024;
            char[] buffer = new char[size];
            int len;
            while ((len = br.read(buffer, 0, size)) > -1) {
                response.write(buffer, 0, len);
            }
            return response.toString();

        } catch (FileNotFoundException e) {
            // ...
            System.out.println("arxiu no existeix");
        } catch (MalformedURLException e) {

        } catch (IOException e) {
            // ..
        }
        return null;
    }
}