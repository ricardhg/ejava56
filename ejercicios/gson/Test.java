import com.google.gson.Gson;


public class Test {
    public static void main(String[] args) {
        Gson gson = new Gson();

        Persona p = new Persona("humbert", 22);
        //gson.toJson(p, System.out);
        
        String str = gson.toJson(p);
        System.out.println(str);

        String js = "{\"nombre\":\"eva\",\"edad\":22}";
        Persona p2 = gson.fromJson(js, Persona.class);
        System.out.println(p2.nombre);
    }
}
