import java.util.Scanner;

class Test {

    public static void main(String[] args) {
        

        int numMotos = 2;
        Moto[] motos =  new Moto[numMotos];
        Scanner teclat =  new Scanner(System.in);
     

        for (int i = 0; i<numMotos; i++) {
            String marca;
            String model;
            int cv;
            double eur;

            //demanar dades a usuari
            System.out.print("Marca: ");
            marca = teclat.nextLine();
            System.out.print("Model: ");
            model = teclat.nextLine();
            System.out.print("Potencia (cv): ");
            cv = teclat.nextInt();
            teclat.nextLine();
            System.out.print("Preu (eur): ");
            eur = teclat.nextDouble();
            teclat.nextLine();
            System.out.println("-----------------");
            
            // crear la moto
            Moto moto = new Moto(marca, model, cv, eur);
            motos[i]=moto;
        }


        System.out.println("---------- TODAS:");
        for (Moto m : motos){
            System.out.println(m);
        }

        System.out.println("---------- MAS POTENTE:");
        System.out.println(MotoUtil.masPotente(motos));
        
        System.out.println("---------- MAS CARA:");
        System.out.println(MotoUtil.masCara(motos));

        
    }
}