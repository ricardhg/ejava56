

class Moto {

    public String model;
    public String marca;
    public int cv;
    public double eur;

    public Moto(String marca, String model, int cv, double eur) {
        this.model = model;
        this.marca = marca;
        this.cv = cv;
        this.eur = eur;
    }

    @Override
    public String toString() {
        return this.model+" "+this.marca+" cv:"+this.cv+" eur:"+this.eur;
    }
   

}