
class MotoUtil {

    public static Moto masPotente(Moto[] motos) {
        int idx = 0;
        int idxMax = 0;
        int valueMax = 0;
        for (Moto m : motos) {
            if (m.cv > valueMax) {
                valueMax = m.cv;
                idxMax = idx;
            }
            idx++;
        }

        // for (int i=0; i<motos.length; i++) {
        //     if (m.cv > valueMax) {
        //         valueMax = m.cv;
        //         idxMax = i;
        //     }
        // }

        return motos[idxMax];
    }

    public static Moto masCara(Moto[] motos) {
        int idx = 0;
        int idxMax = 0;
        double valueMax = 0;
        for (Moto m : motos) {
            if (m.eur > valueMax) {
                valueMax = m.eur;
                idxMax = idx;
            }
            idx++;
        }
        return motos[idxMax];
    }

}