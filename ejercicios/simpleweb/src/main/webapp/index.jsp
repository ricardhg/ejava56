<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.cursjava.simpleweb.MiHtml" %>

<%
    String[] lis = new String[] {"primero", "segundo", "tercero"};

    //MiHtml es una clase local del proyecto simpleweb
    String bloqueUl = MiHtml.stringsUl(lis);
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Ejercicios HMTL/CSS</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

<%= MiHtml.titol("Hola JSP!") %>

<br>
<%= bloqueUl %>
<% out.print(bloqueUl); %>

</body>
</html>
