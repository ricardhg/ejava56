package com.cursjava.simpleweb;

public class MiHtml {

    public static String stringsUl(String[] elementos) {

        StringBuilder sb = new StringBuilder();

        sb.append("<ul>");
        for (String s : elementos) {
            sb.append("<li>"+s+"</li>");
        }
        sb.append("</ul>");

        return sb.toString();
    }

    public static String titol(String text) {
        return "<h4>"+text+"</h4>";
    }

}
