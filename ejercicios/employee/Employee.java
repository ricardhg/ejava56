
class Employee {

    private String forename;
    private String midname;
    private String surname;

    public Employee(String forename, String midname, String surname) {
        this.forename = forename;
        this.midname = midname;
        this.surname = surname;
    }

    public Employee(String forename, String surname) {
        this.forename = forename;
        this.surname = surname;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.forename).append(" ");
        if (this.midname != null) {
            sb.append(this.midname).append(" ");
        }
        sb.append(this.surname);
        return sb.toString();
    }

    public String toXML() {

        StringBuilder sb = new StringBuilder();
        String identat = "\t";
        String newline = "\n\r";

        sb.append("<employee>").append(newline);
        sb.append(identat).append("<username>").append(newline);
        sb.append(identat).append(identat).append(this.getUsername()).append(newline);
        sb.append(identat).append("</username>").append(newline);
        sb.append(identat).append("<fullname>").append(newline);
        sb.append(identat).append(identat).append(this.toString()).append(newline);
        sb.append(identat).append("<fullname>").append(newline);
        sb.append("</employee>").append(newline);

        return sb.toString();

    }

    public String getUsername() {
        return this.forename.substring(0, 1).toLowerCase()
             + this.surname.toLowerCase();
    }

}