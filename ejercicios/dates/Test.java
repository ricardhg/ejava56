
import java.util.Calendar;
import java.util.Date;

class Test {

    public static void main(String[] args) {
        
        Date d = Dateutil.createDate(2018, 7, 7);

        String datastr = Dateutil.formatDate(d, "dd-MMMM-yyyy");
        System.out.println(datastr);

        // Calendar cal = Calendar.getInstance();
        // cal.set(2018, 6, 18);
        // int diaSetmana = cal.get(Calendar.DAY_OF_WEEK);
        // System.out.println("avui es dia: "+diaSetmana);

        int domingos = Dateutil.cuentaDomingos(2018, 7);
        System.out.printf("Total: %d domingos\n", domingos);
    }

}