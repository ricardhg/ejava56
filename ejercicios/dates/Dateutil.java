import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


class Dateutil {

    private Dateutil(){}

    public static Date createDate(int year, int month, int day){
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, day);
        return cal.getTime();
    }

    public static Date createDateTime(int year, int month, int day, int hores, int minuts, int segons){
        Calendar cal = Calendar.getInstance();
        cal.set(year, month-1, day, hores, minuts, segons);
        return cal.getTime();
    }


    public static String formatDate(Date d, String plantilla) {

        SimpleDateFormat sdf =  new SimpleDateFormat(plantilla);
        return sdf.format(d);

    }

    
    public static int cuentaDomingos(int anyo, int mes){
        int domingos = 0;
        int mesCalendar = mes - 1;

        Calendar cal = Calendar.getInstance();
        cal.set(anyo, mesCalendar, 1);
        
        while (cal.get(Calendar.MONTH)==mesCalendar) {
            if (cal.get(Calendar.DAY_OF_WEEK)==1) {
                domingos++;
                System.out.println(cal.getTime());
            }
            cal.add(Calendar.DATE, 1);
        }
        return domingos;
    }



    

}