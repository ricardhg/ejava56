package com.sjava.jdbctest.modelos;

public class Alumno {


    public static final String TABLE = "alumnos";
    public static final String KEY = "idalumnos";
    
    private int idalumnos;
    private String nombre;
    private String email;
    private String telefono;


    public Alumno(int idalumnos, String nombre, String email, String telefono){
        this.idalumnos = idalumnos;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Alumno() {}


    @Override
    public String toString() {
        return String.format("%s [%s]", this.nombre, this.email);
    }


    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the idalumnos
     */
    public int getId() {
        return idalumnos;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param idalumnos the idalumnos to set
     */
    public void setId(int idalumnos) {
        this.idalumnos = idalumnos;
    }


}