package com.sjava.jdbctest.vistas.alumnos;


import com.sjava.jdbctest.modelos.Alumno;
import java.util.List;


public class Lista {

    public static void mostrar (List<Alumno> alumnos) {
        for (Alumno al : alumnos) {
            System.out.println(al.toString());
        }
    }

}