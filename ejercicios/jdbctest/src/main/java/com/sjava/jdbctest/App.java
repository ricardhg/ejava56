package com.sjava.jdbctest;

import java.util.List;

import com.sjava.jdbctest.controladores.AlumnoController;
import com.sjava.jdbctest.modelos.Alumno;
import com.sjava.jdbctest.vistas.alumnos.Lista;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        List<Alumno> lista = AlumnoController.getAll();
        Lista.mostrar(lista);
    }
}
