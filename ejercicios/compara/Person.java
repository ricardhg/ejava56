class Person {

    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("{nombre: %s, edad: %d}", this.name, this.age);
    }
}

