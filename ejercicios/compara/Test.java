import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        List<Person> people = Arrays.asList(
                new Person("Ana", 24),
                new Person("Cris", 18),
                new Person("Bea", 21)
        );

        List<Person> people2 = new ArrayList<Person>();
        Person p1 = new Person("Ana", 24);
        Person p2 = new Person("Cris", 18);
        Person p3 = new Person("Bea", 21);
        people2.add(p1);
        people2.add(p2);
        people2.add(p3);

        Comparator comp = new AgeComparator();
        Collections.sort(people2, comp);
        System.out.println(people2);

        Comparator comp2 = new NameComparator();
        Collections.sort(people2, comp2);
        System.out.println(people2);


        // Collections.sort(people, new NameComparator());
        // System.out.println(people);
        // Collections.sort(people, new AgeComparator());
        // System.out.println(people);

      
    }
}

