import java.util.Comparator;

class AgeComparator implements Comparator<Person> {

    @Override
    public int compare(Person a, Person b) {
        return a.age < b.age ? -1 : a.age == b.age ? 0 : 1;
    }

}
