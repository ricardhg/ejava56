

class Company {
    public String name;
    public int year;

    public Company(String name, int year) {
        this.name = name;
        this.year = year;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + year;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Company other = (Company) obj;
        if (year != other.year) return false;
        if (name == null) {
            if (other.name != null) return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }


    @Override
    public String toString() {
        return this.name + " ("+ this.year + ")";
    }


}