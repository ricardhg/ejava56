<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.cursjava.academia2.controladores.*" %>
<%@page import="com.cursjava.academia2.modelos.*" %>

<%
    boolean datosOk;
    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo profesor
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String telefono = request.getParameter("telefono");

        // aquí validaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // suponemos que todo ok
        datosOk=true;
        if (datosOk){
            Profesor a = new Profesor(nombre, email, telefono);
            ProfesorController.save(a);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo profesor
            response.sendRedirect("/webacademy/profesor/list.jsp");
            return;
        }
    }


%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/webacademy/css/estilos.css">
</head>
<body>

<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
    <div class="col">
        <h1 class="titol">Nuevo profesor</h1>
    </div>
</div>


<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">

  <div class="form-group">
    <label for="nombreInput">Nombre del profesor</label>
    <input  name="nombre"  type="text" class="form-control" id="nombreInput" >
  </div>

  <div class="form-group">
    <label for="emailInput">Email del profesor</label>
    <input  name="email"  type="text" class="form-control" id="emailInput">
  </div>
  
  <div class="form-group">
    <label for="telefonoInput">Teléfono del profesor</label>
    <input  name="telefono"  type="text" class="form-control" id="telefonoInput" >
  </div>

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>



     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/webacademy/js/scripts.js"></script>




</body>
</html>
