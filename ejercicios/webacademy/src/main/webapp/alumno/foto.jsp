<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.cursjava.academia2.controladores.*" %>
<%@page import="com.cursjava.academia2.modelos.*" %>

<!-- 
    IMPORTANTE: hay que añadir a pom.xml la dependencia:
     <dependency>
      <groupId>commons-fileupload</groupId>
      <artifactId>commons-fileupload</artifactId>
      <version>1.3</version>
    </dependency>
    además, es necesario importar las siguientes clases:
    -->

<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>



<%
    //debemos recibir el id del alumno al cual vamos a cambiar la foto,
    //si no no vamos bien...
    String idalumno_s = request.getParameter("id");
    int idalumno = 0;
    if (idalumno_s == null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect( request.getContextPath() +"/index.jsp");
        return;
    } else {
         idalumno = Integer.parseInt(idalumno_s);
         //igualmente, si id_numerico no es válido (>0), nos vamos
         if (idalumno<=0) {
            response.sendRedirect( request.getContextPath() +"/index.jsp");
            return;   
         }
    }

    // solo llegamos aquí si tenemos un idalumno válido! seguimos...
    Alumno al = AlumnoController.getId(idalumno);
    

    File file ;
    int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
    int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria
    
    boolean muestraForm = true;
    String urlImagen = null;

    String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
    String urlBase = request.getContextPath() + pathImagenes;

    // archivoDestino contendrá el nombre del archivo que crearmos
    String archivoDestino = null;

    //getRealPath nos da la ubicación en disco de la carpeta /uploads
    //necesaria para guardar el archivo subido
    //podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat" en linux
    //o cualquier carpeta no protegida de windows
    //en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
    //en este caso suponemos que es la carpeta uploads dentro de webapp
    // IMPORTANTE añadir el "/" al final!

    String realPath = pageContext.getServletContext().getRealPath(pathImagenes)+"/";
    

    // verificamos si estamos recibiendo imagen
    // en dos sentidos: si es POST y si es tipo multipart
    String contentType = request.getContentType();
    if ("POST".equalsIgnoreCase(request.getMethod()) && contentType.indexOf("multipart/form-data") >= 0) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      factory.setSizeThreshold(maxMemSize);
      //por si hiciese falta, indicamos la carpeta tmp
      factory.setRepository(new File(System.getProperty( "java.io.tmpdir" )));
      ServletFileUpload upload = new ServletFileUpload(factory);
      upload.setSizeMax( maxFileSize );
      try{ 
        // ojo, fileItems no sólo incluye archivos, podrian ser también
        // campos INPUT tales como el pie de foto
         List<FileItem> formItems = upload.parseRequest(request);
       
        if (formItems != null && formItems.size() > 0) {
            for (FileItem fi : formItems) {
                if ( !fi.isFormField() )  {
                    if (fi.getSize()>0){
                        System.out.println("un file...");
                        //si no es formfiueld es un FILE
                        //filename es el nombre del archivo que se ha subido
                        String fileName = fi.getName();
                                
                
                        // creamos puntero a archivo ubicado en carpeta real del disco
                        // más nombre de archivoDestino (igual al que se ha subido en este caso)
                        // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                        archivoDestino = fileName;
                        file = new File( realPath + archivoDestino ); 
                        
                        //qué pasa si archivo ya existe? nos inventamos un prefijo
                        //foto.jpg, 1_foto.jpg, 2_foto.jpg...
                        int t = 0;
                        String archivoTemp = archivoDestino;
                        while (file.exists() && !file.isDirectory()){
                            archivoTemp = String.valueOf(++t)+"_"+archivoDestino;
                            file = new File( realPath + archivoTemp ); 
                        }
                        archivoDestino=archivoTemp;

                        // escribimos archivo en disco:
                        fi.write( file ) ;

                        //getContextPath() devuelve nombre app, en este caso: "/academiapp"
                        //guardamos el nombre de la imagen tal como lo requerirá html
                        urlImagen = urlBase + "/" + archivoDestino;
                        //muestraform decide si mostramos el form o la foto
                        muestraForm = false;
                        //guardamos nombre img
                        AlumnoController.setUrlfoto(idalumno, archivoDestino);
                        //AlumnoController.getUrlfoto(idalumno);
                    }else{
                        System.out.println("NO File!");
                    }
            } 
         }
          }

      }catch(Exception ex) {
          //error, redirigimos a listado...
         System.out.println(ex);
       

      }

        response.sendRedirect( request.getContextPath() +"/alumno/list.jsp");
         return;

    }


       %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/academiapp/css/estilos.css">
</head>
<body>

<%@include file="/parts/menu.jsp"%>

<div class="container">
<div class="row">
<div class="col">
<h2>Foto para <%= al.getNombre() %></h2>
</div>
</div>

<div class="row">
<div class="col-md-4">
<img class="img-thumbnail" src="<% out.print( "/webacademy/uploads/"+al.getUrlfoto()); %>" />
</div>
</div>


<div class="row">

<% if (muestraForm) { %>
<div class="col-md-8">

<%-- <form id="formcrea" action="/academiapp/subefoto" method="POST" enctype="multipart/form-data"> --%>
<form id="formcrea" action="#" method="POST" enctype="multipart/form-data">
 <div class="form-group">
    <label for="foto">Fotografia</label>
    <input type="file" class="form-control-file" name="foto" id="foto">
  </div>



    <input type="hidden" name="idalumno" value="<%= idalumno %>">

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
</div>
<% } else { %>

<div class="col-md-4">
<img src="<%= urlImagen %>" class="img-fluid" />
<h4><%= archivoDestino %> </h3>
</div>
<% } %>
</div>
</div>


</body>
</html>
