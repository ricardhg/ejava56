package com.cursjava.academia2.controladores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cursjava.academia2.DBConn;
import com.cursjava.academia2.modelos.Profesor;
import com.cursjava.academia2.modelos.Curso;
import com.mysql.jdbc.Connection;

public class ProfesorController {

    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "profesores";
	private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Profesor> getAll(){
        
        List<Profesor> listaProfesors = new ArrayList<Profesor>();
		String sql = String.format("select %s,nombre,email,telefono from %s", KEY, TABLE);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Profesor u = new Profesor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono")
                );
                listaProfesors.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaProfesors;

    }


    // getId devuelve todos los registros de la tabla
    public static Profesor getId(int idprofesor){
        
        Profesor al = null;
		String sql = String.format("select %s,nombre,email,telefono from %s where id=%d", KEY, TABLE, idprofesor);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                al = new Profesor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono")
                );
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return al;
    }


   
    public static List<Profesor> getAllCurso(Curso curso){
        
        List<Profesor> listaProfesors = new ArrayList<Profesor>();
        String sql = String.format("select a.* from matriculas m, profesors a where m.profesors_id=a.id and m.cursos_id=%d",
                                         curso.getId());
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Profesor u = new Profesor(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono")
                );
                listaProfesors.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaProfesors;

    }

    public static void save(Profesor al) {
        String sql; // (1)
        if (al.getId()>0) { // (2)
            sql = String.format("UPDATE %s set nombre=?, email=?, telefono=? where %s=%d", TABLE, KEY, al.getId());
        }else { // (3)
            sql = String.format("INSERT INTO %s (nombre, email, telefono) VALUES (?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            // (4)
            pstmt.setString(1, al.getNombre());
            pstmt.setString(2, al.getEmail());
            pstmt.setString(3, al.getTelefono());
            pstmt.executeUpdate(); // (5)
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()"); // (6)
                if (rs.next()) { // (7)
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    // (8)
    }
    

    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    

    public static String getNombreById(int id){
        Profesor p = ProfesorController.getId(id);
        if (p==null) {
            return "No assignat";
        } else {
            return p.getNombre();
        }

    }


}