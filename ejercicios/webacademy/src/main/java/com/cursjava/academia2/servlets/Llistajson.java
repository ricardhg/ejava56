package com.cursjava.academia2.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cursjava.academia2.controladores.AlumnoController;
import com.cursjava.academia2.modelos.Alumno;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.google.gson.Gson;



@WebServlet("/llistajson")
public class Llistajson extends HttpServlet {

    private static final long serialVersionUID = -161485L;
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		List<Alumno> llistat = AlumnoController.getAll();

		Gson gson = new Gson();
		String str = gson.toJson(llistat);
		
		response.setContentType("text/json");
		response.setCharacterEncoding("UTF-8");
		
		response.getWriter().append(str);


	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
				response.getWriter().append("hola post");
	}	
	
}