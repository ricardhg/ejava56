package com.cursjava.academia2.modelos;
import java.util.Date;

public class Curso {

    private int id;
    private String nombre;
    private Date fechai;
    private Date fechaf;
    private int profesores_id;
   
    public Curso(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }

    public Curso(int id, String nombre, Date fechai, Date fechaf){
        this.id = id;
        this.nombre = nombre;
        this.fechai = fechai;
        this.fechaf = fechaf;
    }


    public Curso(int id, String nombre, Date fechai, Date fechaf, int profesores_id){
        this.id = id;
        this.nombre = nombre;
        this.fechai = fechai;
        this.fechaf = fechaf;
        this.profesores_id = profesores_id;
    }


    public Curso(String nombre){
        this.nombre = nombre;
    }

    public Curso(String nombre, Date fechai, Date fechaf){
        this.nombre = nombre;
        this.fechai = fechai;
        this.fechaf = fechaf;
    }

    public Curso(String nombre, Date fechai, Date fechaf, int profesores_id){
        this.nombre = nombre;
        this.fechai = fechai;
        this.fechaf = fechaf;
        this.profesores_id = profesores_id;

    }
    public Curso(){}



    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        
        return this.nombre + " (" + this.id +")";
        
    }




    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the fechaf
     */
    public Date getFechaf() {
        return fechaf;
    }

    /**
     * @return the fechai
     */
    public Date getFechai() {
        return fechai;
    }

    /**
     * @param fechaf the fechaf to set
     */
    public void setFechaf(Date fechaf) {
        this.fechaf = fechaf;
    }

    /**
     * @param fechai the fechai to set
     */
    public void setFechai(Date fechai) {
        this.fechai = fechai;
    }


    /**
     * @return the profesores_id
     */
    public int getProfesores_id() {
        return profesores_id;
    }

    /**
     * @param profesores_id the profesores_id to set
     */
    public void setProfesores_id(int profesores_id) {
        this.profesores_id = profesores_id;
    }
    
}