package com.cursjava.academia2.controladores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.cursjava.academia2.DBConn;
import com.cursjava.academia2.modelos.Alumno;
import com.cursjava.academia2.modelos.Curso;
import com.mysql.jdbc.Connection;

public class AlumnoController {

    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "alumnos";
	private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Alumno> getAll(){
        
        List<Alumno> listaAlumnos = new ArrayList<Alumno>();
		String sql = String.format("select %s,nombre,email,telefono, urlfoto from %s", KEY, TABLE);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Alumno u = new Alumno(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono"),
                    rs.getString("urlfoto")
                    
                );
                listaAlumnos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaAlumnos;

    }


    // getId devuelve todos los registros de la tabla
    public static Alumno getId(int idalumno){
        
        Alumno al = null;
		String sql = String.format("select %s,nombre,email,telefono, urlfoto from %s where id=%d", KEY, TABLE, idalumno);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                al = new Alumno(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono"),
                    rs.getString("urlfoto")
                );
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return al;
    }


   
    public static List<Alumno> getAllCurso(Curso curso){
        
        List<Alumno> listaAlumnos = new ArrayList<Alumno>();
        String sql = String.format("select a.* from matriculas m, alumnos a where m.alumnos_id=a.id and m.cursos_id=%d",
                                         curso.getId());
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Alumno u = new Alumno(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getString("email"),
                    rs.getString("telefono")
                );
                listaAlumnos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaAlumnos;

    }

    public static void save(Alumno al) {
        String sql; // (1)
        if (al.getId()>0) { // (2)
            sql = String.format("UPDATE %s set nombre=?, email=?, telefono=? where %s=%d", TABLE, KEY, al.getId());
        }else { // (3)
            sql = String.format("INSERT INTO %s (nombre, email, telefono) VALUES (?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            // (4)
            pstmt.setString(1, al.getNombre());
            pstmt.setString(2, al.getEmail());
            pstmt.setString(3, al.getTelefono());
            pstmt.executeUpdate(); // (5)
        if (al.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()"); // (6)
                if (rs.next()) { // (7)
                    al.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    // (8)
    }
    

    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
        // asignaFoto asigna URLde FOTO a alumno por id
        public static void setUrlfoto(int id, String url){
            String sql = String.format("UPDATE %s SET urlfoto=? where %s=?", TABLE, KEY);
            try (Connection conn = DBConn.getConn();
            PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, url);
                pstmt.setInt(2, id);
                pstmt.executeUpdate();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }


        public static String getUrlfoto(int id){
            Alumno al = AlumnoController.getId(id);
            if (al!=null) {
                return al.getUrlfoto();
            }else{
                return "";
            }
            
        }
        


}