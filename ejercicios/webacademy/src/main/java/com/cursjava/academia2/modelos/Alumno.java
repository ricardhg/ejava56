package com.cursjava.academia2.modelos;


public class Alumno {

    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private String urlfoto;

    public Alumno(int id, String nombre, String email, String telefono){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Alumno(int id, String nombre, String email, String telefono, String foto){
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.urlfoto = foto;
    }



    public Alumno(String nombre, String email, String telefono){
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
    }

    public Alumno(){}

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }


    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        
        return this.nombre + " (" + this.email +")";
        
    }


    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    /**
     * @return the urlfoto
     */
    public String getUrlfoto() {
        return this.urlfoto;
    }

    /**
     * @param urlfoto the urlfoto to set
     */
    public void setUrlfoto(String urlfoto) {
        this.urlfoto = urlfoto;
    }

}