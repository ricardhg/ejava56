package com.cursjava.academia2.controladores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cursjava.academia2.DBConn;
import com.cursjava.academia2.modelos.*;
import com.mysql.jdbc.Connection;

public class CursoController {

    // constantes utilizadas en las ordenes sql
	private static final String TABLE = "cursos";
	private static final String KEY = "id";

    // getAll devuelve todos los registros de la tabla
    public static List<Curso> getAll(){
        
        List<Curso> listaCursos = new ArrayList<Curso>();
		String sql = String.format("select %s,nombre, fechai, fechaf, profesores_id from %s", KEY, TABLE);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Curso u = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getDate("fechai"),
                    rs.getDate("fechaf"),
                    rs.getInt("profesores_id")
                );
                listaCursos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaCursos;

    }


    // getId devuelve todos los registros de la tabla
    public static Curso getId(int idalumno){
        
        Curso al = null;
		String sql = String.format("select %s,nombre, fechai, fechaf from %s where id=%d", KEY, TABLE, idalumno);
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                al = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre"),
                    rs.getDate("fechai"),
                    rs.getDate("fechaf")
                );
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return al;

    }


    public static List<Curso> getAllCursos(Alumno al){
 
        List<Curso> listaCursos = new ArrayList<Curso>();
        String sql = String.format("select c.* from matriculas m, cursos c where m.cursos_id=c.id and m.alumnos_id=%d",
                                         al.getId());

		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Curso u = new Curso(
                    rs.getInt(KEY),
                    rs.getString("nombre")
                );
                listaCursos.add(u);
            }
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaCursos;
    }





    //save guarda un alumno
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Curso curs) {
        String sql;
        if (curs.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, fechai=?, fechaf=?, profesores_id=? where %s=%d", TABLE, KEY, curs.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, fechai, fechaf, profesores_id) VALUES (?,?,?,?)", TABLE);
        }

            
        try (Connection conn = DBConn.getConn();
        PreparedStatement pstmt = conn.prepareStatement(sql);
        Statement stmt = conn.createStatement()) {
            pstmt.setString(1, curs.getNombre());
            pstmt.setInt(4, curs.getProfesores_id());

            // ojo! la clase que devuelve getFechai es un java.util.Date
            // en cambio, el método setDate espera una fecha de tipo java.sql.Date
            // tenemos que hacer la conversión de una a otra, la forma más fácil es convertir
            // la primera en milisegundos y crear un objeto de la segunda a partir de éstos
            long ms = curs.getFechai().getTime();
            java.sql.Date fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(2, fecha_sql);
            
            ms = curs.getFechaf().getTime();
            fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(3, fecha_sql);
          
            pstmt.executeUpdate();

        if (curs.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    curs.setId(rs.getInt(1));
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


        /*
    método que a partir de una fecha en formato aaaa-mm-dd devuelve
    un objeto java.util.Date
    sirve por ejemplo para convertir una fecha recibida desde un formulario web
    a una fecha adecuada para setDate de preparedstatement
    */
    public static Date stringToDate(String sdata){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        Date theDate = null;
            try {
                theDate = df.parse(sdata);
            } catch (ParseException e) {
                //e.printStackTrace();
            }
        return theDate;
    }


    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}