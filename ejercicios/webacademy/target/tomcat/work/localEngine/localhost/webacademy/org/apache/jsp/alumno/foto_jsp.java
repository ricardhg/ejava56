package org.apache.jsp.alumno;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.cursjava.academia2.controladores.*;
import com.cursjava.academia2.modelos.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.io.output.*;

public final class foto_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/parts/menu.jsp");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!-- \r\n");
      out.write("    IMPORTANTE: hay que añadir a pom.xml la dependencia:\r\n");
      out.write("     <dependency>\r\n");
      out.write("      <groupId>commons-fileupload</groupId>\r\n");
      out.write("      <artifactId>commons-fileupload</artifactId>\r\n");
      out.write("      <version>1.3</version>\r\n");
      out.write("    </dependency>\r\n");
      out.write("    además, es necesario importar las siguientes clases:\r\n");
      out.write("    -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

    //debemos recibir el id del alumno al cual vamos a cambiar la foto,
    //si no no vamos bien...
    String idalumno_s = request.getParameter("id");
    int idalumno = 0;
    if (idalumno_s == null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect( request.getContextPath() +"/index.jsp");
        return;
    } else {
         idalumno = Integer.parseInt(idalumno_s);
         //igualmente, si id_numerico no es válido (>0), nos vamos
         if (idalumno<=0) {
            response.sendRedirect( request.getContextPath() +"/index.jsp");
            return;   
         }
    }

    // solo llegamos aquí si tenemos un idalumno válido! seguimos...
    Alumno al = AlumnoController.getId(idalumno);
    

    File file ;
    int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
    int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria
    
    boolean muestraForm = true;
    String urlImagen = null;

    String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
    String urlBase = request.getContextPath() + pathImagenes;

    // archivoDestino contendrá el nombre del archivo que crearmos
    String archivoDestino = null;

    //getRealPath nos da la ubicación en disco de la carpeta /uploads
    //necesaria para guardar el archivo subido
    //podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat" en linux
    //o cualquier carpeta no protegida de windows
    //en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
    //en este caso suponemos que es la carpeta uploads dentro de webapp
    // IMPORTANTE añadir el "/" al final!

    String realPath = pageContext.getServletContext().getRealPath(pathImagenes)+"/";
    

    // verificamos si estamos recibiendo imagen
    // en dos sentidos: si es POST y si es tipo multipart
    String contentType = request.getContentType();
    if ("POST".equalsIgnoreCase(request.getMethod()) && contentType.indexOf("multipart/form-data") >= 0) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      factory.setSizeThreshold(maxMemSize);
      //por si hiciese falta, indicamos la carpeta tmp
      factory.setRepository(new File(System.getProperty( "java.io.tmpdir" )));
      ServletFileUpload upload = new ServletFileUpload(factory);
      upload.setSizeMax( maxFileSize );
      try{ 
        // ojo, fileItems no sólo incluye archivos, podrian ser también
        // campos INPUT tales como el pie de foto
         List<FileItem> formItems = upload.parseRequest(request);
       
        if (formItems != null && formItems.size() > 0) {
            
            for (FileItem fi : formItems) {
             
          
                if ( !fi.isFormField() )  {
               

                if (fi.getSize()>0){
                
                    System.out.println("un file...");
                    //si no es formfiueld es un FILE
                    //filename es el nombre del archivo que se ha subido
                    String fileName = fi.getName();
                            
            
                    // creamos puntero a archivo ubicado en carpeta real del disco
                    // más nombre de archivoDestino (igual al que se ha subido en este caso)
                    // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                    archivoDestino = fileName;
                    file = new File( realPath + archivoDestino ); 
                    
                    //qué pasa si archivo ya existe? nos inventamos un prefijo
                    //foto.jpg, 1_foto.jpg, 2_foto.jpg...
                    int t = 0;
                    String archivoTemp = archivoDestino;
                    while (file.exists() && !file.isDirectory()){
                        archivoTemp = String.valueOf(++t)+"_"+archivoDestino;
                        file = new File( realPath + archivoTemp ); 
                    }
                    archivoDestino=archivoTemp;

                    // escribimos archivo en disco:
                    fi.write( file ) ;

                    //getContextPath() devuelve nombre app, en este caso: "/academiapp"
                    //guardamos el nombre de la imagen tal como lo requerirá html
                    urlImagen = urlBase + "/" + archivoDestino;
                    //muestraform decide si mostramos el form o la foto
                    muestraForm = false;
                    //guardamos nombre img
                    AlumnoController.setUrlfoto(idalumno, archivoDestino);
                    //AlumnoController.getUrlfoto(idalumno);
                }else{
                    System.out.println("NO File!");
                }
            } 
         }
          }

      }catch(Exception ex) {
          //error, redirigimos a listado...
         System.out.println(ex);
       

      }

        response.sendRedirect( request.getContextPath() +"/alumno/list.jsp");
         return;

    }


       
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    <title>AcademiaApp</title>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css\" integrity=\"sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4\" crossorigin=\"anonymous\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/academiapp/css/estilos.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-expand-lg  navbar-dark bg-primary\">\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">WebAcademy</a>\r\n");
      out.write("    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("      <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("  \r\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("      <ul class=\"navbar-nav mr-auto\">\r\n");
      out.write("        <li class=\"nav-item active\">\r\n");
      out.write("          <a class=\"nav-link\" href=\"/webacademy\">Inicio</a>\r\n");
      out.write("        </li>\r\n");
      out.write("    \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Alumnos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/alumno/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/alumno/create.jsp\">Nuevo Alumno</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("      \r\n");
      out.write("      <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Profesores\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/profesor/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/profesor/create.jsp\">Nuevo Alumno</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("      \r\n");
      out.write("            \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Cursos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/curso/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/curso/create.jsp\">Nuevo Curso</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("      </ul>\r\n");
      out.write("      \r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("  </nav>\r\n");
      out.write("  \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col\">\r\n");
      out.write("<h2>Foto para ");
      out.print( al.getNombre() );
      out.write("</h2>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");

System.out.println(al.getUrlfoto() + al.getNombre());

      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col-md-4\">\r\n");
      out.write("<img class=\"img-thumbnail\" src=\"");
 out.print( "/webacademy/uploads/"+al.getUrlfoto()); 
      out.write("\" />\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("\r\n");
 if (muestraForm) { 
      out.write("\r\n");
      out.write("<div class=\"col-md-8\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<form id=\"formcrea\" action=\"#\" method=\"POST\" enctype=\"multipart/form-data\">\r\n");
      out.write(" <div class=\"form-group\">\r\n");
      out.write("    <label for=\"foto\">Fotografia</label>\r\n");
      out.write("    <input type=\"file\" class=\"form-control-file\" name=\"foto\" id=\"foto\">\r\n");
      out.write("  </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <input type=\"hidden\" name=\"idalumno\" value=\"");
      out.print( idalumno );
      out.write("\">\r\n");
      out.write("\r\n");
      out.write("    <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\r\n");
      out.write("</form>\r\n");
      out.write("</div>\r\n");
 } else { 
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"col-md-4\">\r\n");
      out.write("<img src=\"");
      out.print( urlImagen );
      out.write("\" class=\"img-fluid\" />\r\n");
      out.write("<h4>");
      out.print( archivoDestino );
      out.write(" </h3>\r\n");
      out.write("</div>\r\n");
 } 
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
