package org.apache.jsp.alumno;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.cursjava.academia2.controladores.*;
import com.cursjava.academia2.modelos.*;

public final class list_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/parts/menu.jsp");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("  <head>\r\n");
      out.write("    <!-- Required meta tags -->\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n");
      out.write("\r\n");
      out.write("    <!-- Bootstrap CSS -->\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/webacademy/css/estilos.css\">\r\n");
      out.write("\r\n");
      out.write("    <title>WebAcademy</title>\r\n");
      out.write("  </head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-expand-lg  navbar-dark bg-primary\">\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">WebAcademy</a>\r\n");
      out.write("    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("      <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("  \r\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("      <ul class=\"navbar-nav mr-auto\">\r\n");
      out.write("        <li class=\"nav-item active\">\r\n");
      out.write("          <a class=\"nav-link\" href=\"/webacademy\">Inicio</a>\r\n");
      out.write("        </li>\r\n");
      out.write("    \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Alumnos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/alumno/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/alumno/create.jsp\">Nuevo Alumno</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("      \r\n");
      out.write("      <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Profesores\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/profesor/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/profesor/create.jsp\">Nuevo Alumno</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("      \r\n");
      out.write("            \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Cursos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/curso/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/webacademy/curso/create.jsp\">Nuevo Curso</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("      </ul>\r\n");
      out.write("      \r\n");
      out.write("\r\n");
      out.write("    </div>\r\n");
      out.write("  </nav>\r\n");
      out.write("  \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col\">\r\n");
      out.write("<h1 class=\"titol\">Listado</h1>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<style>\r\n");
      out.write("img.peque {\r\n");
      out.write("  width: 100px;\r\n");
      out.write("}\r\n");
      out.write("</style>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<table class=\"table\" id=\"tabla_listado\">\r\n");
      out.write("  <thead>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <th scope=\"col\">#</th>\r\n");
      out.write("      <th scope=\"col\"></th>\r\n");
      out.write("      <th scope=\"col\">Nombre</th>\r\n");
      out.write("      <th scope=\"col\">Email</th>\r\n");
      out.write("      <th scope=\"col\">Teléfono</th>\r\n");
      out.write("      <th scope=\"col\"></th>\r\n");
      out.write("      <th scope=\"col\"></th>\r\n");
      out.write("    </tr>\r\n");
      out.write("  </thead>\r\n");
      out.write("  <tbody>\r\n");
      out.write("\r\n");
      out.write("   ");
 for (Alumno al : AlumnoController.getAll()) { 
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("        <th scope=\"row\">");
      out.print( al.getId() );
      out.write("</th>\r\n");
      out.write("        <th><img class=\"peque\" src=\"");
 out.print( "/webacademy/uploads/"+al.getUrlfoto()); 
      out.write("\" /></th>\r\n");
      out.write("        <td>");
      out.print( al.getNombre() );
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print( al.getEmail() );
      out.write("</td>\r\n");
      out.write("        <td>");
      out.print( al.getTelefono() );
      out.write("</td>\r\n");
      out.write("        <td><a href=\"/webacademy/alumno/edit.jsp?id=");
      out.print( al.getId() );
      out.write("\">Edita</a></td>\r\n");
      out.write("        <td><a href=\"/webacademy/alumno/remove.jsp?id=");
      out.print( al.getId() );
      out.write("\">Elimina</a></td>\r\n");
      out.write("        <td><a href=\"/webacademy/alumno/foto.jsp?id=");
      out.print( al.getId() );
      out.write("\">Imagen</a></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("    ");
 } 
      out.write("\r\n");
      out.write("  </tbody>\r\n");
      out.write("</table>\r\n");
      out.write("\r\n");
      out.write("<a href=\"/webacademy/alumno/create.jsp\" class=\"btn btn-sm btn-danger\">Nuevo alumno</a>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("     <!-- Optional JavaScript -->\r\n");
      out.write("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"/webacademy/js/scripts.js\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
