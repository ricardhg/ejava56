<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.cursjava.academia2.controladores.*" %>
<%@page import="com.cursjava.academia2.modelos.*" %>



<!DOCTYPE html>
<html lang="es-ES">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/webacademy/css/estilos.css">

    <title>WebAcademy</title>
  </head>
<body>

<%@include file="/parts/menu.jsp"%>


<div class="container">

<div class="row">
<div class="col">
<h1 class="titol">Listado</h1>
</div>
</div>


<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Teléfono</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>

   <% for (Alumno al : AlumnoController.getAll()) { %>
        <tr>
        <th scope="row"><%= al.getId() %></th>
        <td><%= al.getNombre() %></td>
        <td><%= al.getEmail() %></td>
        <td><%= al.getTelefono() %></td>
        <td><a href="/webacademy/alumno/edit.jsp?id=<%= al.getId() %>">Edita</a></td>
        <td><a href="/webacademy/alumno/remove.jsp?id=<%= al.getId() %>">Elimina</a></td>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/webacademy/alumno/create.jsp" class="btn btn-sm btn-danger">Nuevo alumno</a>
</div>
</div>


</div>


     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/webacademy/js/scripts.js"></script>


</body>
</html>
