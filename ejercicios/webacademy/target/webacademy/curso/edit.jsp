<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="com.cursjava.academia2.controladores.*" %>
<%@page import="com.cursjava.academia2.modelos.*" %>
<%@page import="java.util.Date" %>



<%

    Curso al = null;
    boolean datos_guardados=false;

    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/webacademy");
        //IMPORTANTE! después de sendRedirect poner un RETURN!!!
        return;
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String nombre = request.getParameter("nombre");
                String fechai = request.getParameter("fechai");
                String fechaf = request.getParameter("fechaf");

                Date d_fechai = CursoController.stringToDate(fechai);
                Date d_fechaf = CursoController.stringToDate(fechaf);

                //creamos nuevo objeto curso, que reemplazará al actual del mismo id
                al = new Curso(id_numerico,nombre,d_fechai, d_fechaf);
                CursoController.save(al);
                datos_guardados=true;
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/webacademy/curso/list.jsp");
                return;
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del curso para mostrarlos en el formulario de edifión
            al = CursoController.getId(Integer.parseInt(id));
            if (al==null) {
                    response.sendRedirect("/webacademy");
                    return;
            } 
        }
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/webacademy/css/estilos.css">
    <title>WebAcademy</title>
  </head>
<body>

<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1 class="titol">Editar curso</h1>
</div>
</div>

<div class="row">
<div class="col-md-8">

<form action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre del curso</label>
    <input  name="nombre"  type="text" class="form-control" id="nombreInput" value="<%= al.getNombre() %>">
  </div>
  <div class="form-group">
    <label for="fechai">Fecha de inicio</label>
    <input  name="fechai"  type="date" class="form-control" id="fechai" value="<%= al.getFechai() %>">
  </div>

  <div class="form-group">
    <label for="fechaf">Fecha de inicio</label>
    <input  name="fechaf"  type="date" class="form-control" id="fechaf" value="<%= al.getFechaf() %>">
  </div>
   
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%= al.getId() %>">
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
<br>
<br>
<br>
<% if (datos_guardados==true) { %>

<h1 class="rojo">Datos guardados!</h1>
<%}%>

</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/webacademy/js/scripts.js"></script>



</body>
</html>
