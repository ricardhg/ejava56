<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.cursjava.academia2.controladores.*" %>
<%@page import="com.cursjava.academia2.modelos.*" %>

<%

    String id = request.getParameter("id");
    if (id==null) {
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/webacademy/index.jsp");
    } else {
        int id_numerico = Integer.parseInt(id);
        CursoController.removeId(id_numerico);
        response.sendRedirect("/webacademy/curso/list.jsp");
    }

%>
