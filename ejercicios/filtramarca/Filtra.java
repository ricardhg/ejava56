import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

class Filtra {

    public static void run(String fileName, String marca) throws Exception {
      
        File inFile = new File(fileName);
        File outFile = new File(marca+".csv");

        FileReader fr = new FileReader(inFile);
        BufferedReader br = new BufferedReader(fr);

        FileWriter fw = new FileWriter(outFile);
        BufferedWriter bw = new BufferedWriter(fw);

        String line = br.readLine();
        boolean match=false;
        while (line != null) {
            if (line.toLowerCase().contains(marca.toLowerCase())){
                bw.write(line);
                match=true;
            }else{
                match=false;
            }
            line = br.readLine();
            if (line != null && match) {
                bw.newLine();
            }
        }
        bw.flush();
        bw.close();

    }

}