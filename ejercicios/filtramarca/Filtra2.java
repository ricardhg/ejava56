import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

class Filtra2 {

    public static void run(String fileName, String marca)  {
      
        File inFile = new File(fileName);
        File outFile = new File(marca+".csv");

        try (
            FileReader fr = new FileReader(inFile);
            FileWriter fw = new FileWriter(outFile);
            BufferedReader br = new BufferedReader(fr);
            BufferedWriter bw = new BufferedWriter(fw)
            ) {
                String line = br.readLine();
                boolean match=false;
                while (line != null) {
                    if (line.toLowerCase().contains(marca.toLowerCase())){
                        bw.write(line);
                        match=true;
                    }else{
                        match=false;
                    }
                    line = br.readLine();
                    if (line != null && match) {
                        bw.newLine();
                    }
            }
            bw.flush();
        } catch (FileNotFoundException e){
            //...
            System.out.println("arxiu no existeix");
        } catch (IOException e) {
            //..
        }
    }
}
